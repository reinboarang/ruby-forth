# prev word
# name length, name
# primitive?
# execution type: normal, compile-only, immediate
# code

require './primitives.rb'
require './helper.rb'

module Forth
  module ExecType
   NORMAL = 1
   COMPILE_ONLY = 2
   IMMEDIATE = 3
  end

  def self.string_to_counted_string str
    [ str.length ] + str.bytes.to_a
  end

  def self.counted_string_to_string cstr
    raise "counted string must be an array. <#{cstr.inspect}>" unless cstr.is_a?(Array)
    str_len = cstr[0]
		if cstr[1..-1].length < str_len
			puts "INDICATED LEN: #{str_len} ACTUAL LEN: #{cstr[1..-1].length} STRING: #{cstr[1..-1].map { |c| c.is_a?(Integer) ? c.chr : 255.chr }.join('')}"
			raise "insufficient length of counted string."
		end
    cstr[1..str_len].map { |c| c.chr }.join
  end

  class DictionaryEntry
		attr_accessor :dict
		attr_reader :last_word, :name, :primitive, :exec_type, :pos, :xt_pos

    def initialize *args
      case args.size
      when 1
				init_with_dict args[0]
      when 2
				init_with_offset args[0], args[1]
      when 7
				init_raw args[0], args[1], args[2], args[3]
      else
				raise "Invalid number of arguments."
      end
    end

		def valid?
			@pos > -1 && @xt_pos > -1 && @exec_type > 0 && @exec_type < 4
		end

		def exec_type= val
			@dict.bytes[@pos + 3 + @name.length] = val
		end

    private

    def init_raw dict, last_word, name, primitive, exec_type, pos, xt_pos
      @dict, @last_word, @name, @primitive, @exec_type, @pos, @xt_pos = dict, last_word, name, primitive, exec_type, pos, xt_pos
    end

    def init_with_offset dict, offset
			@dict = dict
			@pos = offset
      @last_word = dict.bytes[offset]
      name_len = dict.bytes[offset+1]
      @name = Forth.counted_string_to_string(dict.bytes[offset+1..offset+2+name_len])
      @primitive = dict.bytes[offset+2 + @name.length] == 1
      @exec_type = dict.bytes[offset+3 + @name.length]
			@xt_pos = offset + 4 + @name.length
    end

    def init_with_dict dict
      init_with_offset dict, 0
    end

  end

  class Dictionary
		include Enumerable
    @@PAD_OFFSET = 100
    attr_accessor :bytes, :here, :last_word, :last_xt

    def initialize
      @here = 0
      @last_word = -1
			@last_xt = -1
      @bytes = []
    end

		def each
			cur_last_word = @last_word
			while true do
				break if cur_last_word < 0
				de = DictionaryEntry.new(self, cur_last_word)
				yield de
				cur_last_word = de.last_word
			end
		end

    def [] index
      allot(index - @bytes.size + 1) if index >= @bytes.size
      @bytes[index]
    end

    def []= index, value
      allot(index - @bytes.size + 1) if index >= @bytes.size
      @bytes[index] = value
    end

    def allot size
      @bytes = @bytes + Array.new(size, 0)
    end

    def search name
      lw = @last_word
      loop do
				de = DictionaryEntry.new(self, lw)
				return lw if de.name == name || lw == -1
				lw = @bytes[lw]
      end 
    end

    def

    def pad
      @here + @@PAD_OFFSET
    end

    def append val
      if val.is_a?(Array) then
				@bytes + val
      else
				@bytes[@here] = val
      end
      @here = @bytes.size 
    end

    def insert_here val
      insert_at @here, val
      case val
      when Integer
				@here += 1
      when Proc
        @here += 1
      when Array
				@here += val.size
      end
    end

    def insert_at addr, val
      if val.is_a?(Array) then
				val.map.with_index do |x, i|
					self[addr + i] = x
				end
      else
				self[addr] = val
      end
    end	

    def create_header name, primitive, exec_type
      insert_here @last_word
      @last_word = @here - 1
      insert_here Forth.string_to_counted_string(name)
      insert_here (primitive ? 1 : 0)
      insert_here exec_type
			@last_xt = @here
    end

    def get_xt name
      word_pos = search name
      return word_pos if word_pos == -1
      skip_name = @bytes[word_pos+1]
      word_pos + skip_name + 4
    end

		def get_name_from_xt xt
			each do |e|
				return e.name if xt == e.xt_pos
			end
			return ""
		end

		def disassemble_colon xt
			trace = []
			case xt
			when Integer
				cur_pos = xt + 1
			when String
				cur_pos = get_xt(xt) + 1
			end
			begin
				cur_word = get_name_from_xt(@bytes[cur_pos])
				trace << (cur_word == "" ? @bytes[cur_pos] : cur_word)
				cur_pos += 1
			end while cur_word != "exit"
			trace
		end

		def vocab
			map { |e| e.name }
		end

  end

  class Interpreter
    include CoreVocabulary

    attr_accessor :pc, :wp, :ip
    attr_accessor :dictionary
    attr_accessor :data_stack, :return_stack

    def initialize
			hard_reset
    end

    def reset
      @data_stack = []
      @return_stack = []
      @pc = @wp = @ip = -1
    end

		def hard_reset
			@dictionary = Dictionary.new
			init_vocab
			reset
			int_call "init-input"
		end

		def stack_trace
			@return_stack.map do |xt|
				name = @dictionary.get_name_from_xt(@dictionary[xt])
				puts "xt pointer: #{xt} xt: #{name}"
			end
		end

    def int_next
      return if @pc == -1
      @wp = @pc
      @pc += 1
      @wp = @dictionary.bytes[@wp]
			begin
				@dictionary.bytes[@wp].call(self)
			rescue => e
				puts e.message
				puts e.backtrace
				puts "PC: #{@pc} WP: #{@wp}"
				puts stack_trace.inspect
			end
    end

    def int_enter
      @return_stack.push(@pc)
			@pc = @wp + 1
    end

		def int_call xt
			case xt
			when String
				if @dictionary.search(xt) == -1
					puts "Word not found: \"#{xt}\""
				else
					int_call(@dictionary.get_xt(xt))
				end
			when Integer
				@return_stack = []
				@return_stack.push(@pc)
				@pc = -1
				@wp = xt
				@dictionary[xt].call(self)
			end
		end

		def run
			int_call "colon_init"
		end

		def run_file filename
			tib = get_variable("tib")
			File.open(filename, 'r').readlines.each do |l|
				set_variable(">in", tib)
				raise "Error: Line greater than 80 columns." if l.length > 80
				@dictionary.insert_at(tib, l.bytes)
				@data_stack.push(' '.ord)
				int_call("quit")
			end
		end

    def add_primitive(name, exec_type = ExecType::NORMAL, &proc)
      raise "No block passed." unless block_given?
			entry_pos = @dictionary.here
      @dictionary.create_header name, true, exec_type
      @dictionary.insert_here lambda { |int| proc.call(int); int_next }
			DictionaryEntry.new(@dictionary, entry_pos)
    end

    def add_colon(name, *xts)
			entry_pos = @dictionary.here
      @dictionary.create_header name, false, ExecType::NORMAL
      new_xts =  xts + [ "exit" ]
      @dictionary.insert_here lambda { |int| int_enter; int_next }
      @dictionary.insert_here new_xts.map { |x| x.is_a?(String) ? @dictionary.get_xt(x) : x }
			DictionaryEntry.new(@dictionary, entry_pos)
    end

    def add_variable(name)
			entry_pos = @dictionary.here
      @dictionary.create_header name, false, ExecType::NORMAL
      var_addr = @dictionary.here + 1
      var_lambda = lambda { |addr, int| int.data_stack.push addr; int_next }.curry.(var_addr)
      @dictionary.insert_here var_lambda
      @dictionary.insert_here 0
			DictionaryEntry.new(@dictionary, entry_pos)
    end

    def set_variable(name, val)
      @dictionary[@dictionary.get_xt(name) + 1] = val
    end
    
    def get_variable(name)
      @dictionary[@dictionary.get_xt(name) + 1]
    end

		def clear_tib
			tib = get_variable("tib")
			80.times do |i|
				@dictionary[tib + i] = 0
			end
		end

		def tib_to_string
			tib = get_variable("tib")
			tib_end = tib + 79
			@dictionary.bytes[tib..tib_end].map { |c| c.chr }.join('')
		end

		def to_in_to_string
			tib = get_variable("tib")
			tib_end = tib + 79
			to_in = get_variable(">in")
			@dictionary.bytes[to_in..tib_end].select { |c| c >= 32 && c <= 127 }.map { |c| c.chr }.join('')
		end

    def add_constant(name, val)
			entry_pos = @dictionary.here
      @dictionary.create_header name, false, ExecType::NORMAL
      con_lambda = lambda { |nval, int| int.data_stack.push nval; int_next }.curry.(val)
      @dictionary.insert_here con_lambda
			DictionaryEntry.new(@dictionary, entry_pos)
    end
  end

end

