class String
	def numeric?
		Float(self) != nil rescue false
	end
end

def hard_parse int, delim
	raise "Delimiter must be a character ordinal number" if !delim.is_a?(Integer)
	tib = int.get_variable("tib")
	tib_end = tib + 80
	to_in = int.get_variable(">in")
	while int.dictionary[to_in] == delim
		to_in += 1
	end
	result_addr = to_in
	while cur_char = int.dictionary[to_in] != delim && cur_char != 0 && cur_char != "\n".ord do
		break if to_in >= tib_end
		to_in += 1
		int.set_variable(">in", to_in + 1)
	end

	{ 
		string: Forth.counted_string_to_string([ to_in - result_addr ] + @dictionary.bytes[result_addr..to_in]),
	  addr: result_addr,
	  length: (to_in - result_addr)
	}
end
