module Forth

  module CoreVocabulary
    def init_vocab

      add_primitive("init") do |int|
				int.set_variable("state", 0)
				int.data_stack = []
				int.return_stack = []
				int.pc = -1
				int.wp = -1
      end

      add_primitive("docol") do |int|
				int.int_enter
      end

      add_primitive("dolit") do |int|
				val = int.dictionary[int.pc]
				int.pc+=1
				int.data_stack.push(val)
      end

			add_primitive("depth") do |int|
				int.data_stack.push(int.data_stack.size)
			end

      add_primitive("bye") do |int|
				int.reset
      end

      add_primitive("exit") do |int|
				int.pc = int.return_stack.pop
      end

      add_primitive("branch") do |int|
				int.pc = int.data_stack.pop
      end

      add_primitive("?branch") do |int|
				addr = int.data_stack.pop
				bool = int.data_stack.pop
				int.pc = addr if bool != 0
      end

			add_primitive("create") do |int|
				name = hard_parse(int, ' '.ord)[:string].strip
				int.dictionary.create_header name, false, ExecType::NORMAL
				var_addr = @dictionary.here + 1
				var_lambda = lambda { |addr, new_int| new_int.data_stack.push addr; int_next }.curry.(var_addr)
				int.dictionary.insert_here var_lambda
			end

			add_primitive("code$") do |int|
				code_string = hard_parse(int, '$'.ord)[:string]
				code_lambda = eval "lambda { |interpreter| #{code_string} }"
				int.dictionary.insert_here code_lambda
			end

			add_primitive("immediate") do |int|
			  int.dictionary[int.dictionary.last_xt - 1] = Forth::ExecType::IMMEDIATE
			end.exec_type = Forth::ExecType::IMMEDIATE

      add_primitive(",") do |int|
				int.dictionary.insert_here int.data_stack.pop
      end

      add_primitive("@") do |int|
				raise "Data stack empty!" if int.data_stack == []
				int.data_stack.push(int.dictionary[int.data_stack.pop])
      end

      add_primitive("!") do |int|
				addr = int.data_stack.pop
				int.dictionary[addr] = int.data_stack.pop
      end

			add_primitive("allot") do |int|
				n = int.data_stack.pop
				int.dictionary.here += n
			end

      add_primitive(">r") do |int|
				int.return_stack.push(int.data_stack.pop)
      end

      add_primitive("r>") do |int|
				int.data_stack.push(int.return_stack.pop)
      end

      add_primitive("r@") do |int|
				int.data_stack.push(int.return_stack[-1])
      end

      add_primitive("pick") do |int|
				n = int.data_stack.pop
				int.data_stack.push(int.data_stack[-(n+1)])
      end

      add_colon("dup", "dolit", 0, "pick")

			add_colon("over", "dolit", 1, "pick")

			add_colon("swap", "over", ">r", ">r", "drop", "r>", "r>")

      add_primitive("drop") do |int|
				int.data_stack.pop
      end

      add_primitive("+") do |int|
				int.data_stack.push( int.data_stack.pop + int.data_stack.pop )
      end

      add_primitive("-") do |int|
				a = int.data_stack.pop
				b = int.data_stack.pop
				int.data_stack.push( b - a )
      end

      add_primitive("*") do |int|
				int.data_stack.push( int.data_stack.pop * int.data_stack.pop )
      end

			add_primitive("not") do |int|
				int.data_stack.push( (int.data_stack.pop == 0) ? 1 : 0 )
			end

			add_primitive("=") do |int|
				int.data_stack.push( (int.data_stack.pop == int.data_stack.pop) ? 1 : 0 )
			end

			add_primitive("<") do |int|
				int.data_stack.push( (int.data_stack.pop > int.data_stack.pop) ? 1 : 0 )
			end

			add_primitive(">") do |int|
				int.data_stack.push( (int.data_stack.pop < int.data_stack.pop) ? 1 : 0 )
			end

      add_primitive("emit") do |int|
				putc int.data_stack.pop.chr
      end

      add_primitive(".") do |int|
				printf "%s", int.data_stack.pop
      end

      add_primitive("here") do |int|
				int.data_stack.push int.dictionary.here
      end

      add_variable("tib")
      set_variable("tib", -1)

      add_variable(">in")
      set_variable(">in", -1)

      add_variable("#tib")
      set_variable("#tib", -1)

			add_variable("state")
			set_variable("state", 0)

      add_colon("init-input", "here", "tib", "!",
								              "dolit", 80, "allot",
							 								"dolit", 0, "#tib", "!")

			add_primitive("does>") do |int|
				if int.get_variable("state") == 0 # interpret mode
					last_xt = int.dictionary.last_xt
					last_lambda = int.dictionary[last_xt]
					does_addr = int.data_stack.pop
					new_lambda = lambda do |addr, orig_lambda, other_int|
						orig_lambda.call(other_int)
						other_int.pc = addr
						other_int.int_next
					end.curry.(does_addr, last_lambda)
					int.dictionary[last_xt] = new_lambda 
				else # compile mode
					int.dictionary.insert_here(int.dictionary.get_xt("dolit"))
					int.dictionary.insert_here(int.dictionary.here + 3)
					int.dictionary.insert_here(int.dictionary.get_xt("does>"))
					int.dictionary.insert_here(int.dictionary.get_xt("exit"))
				end
			end.exec_type = Forth::ExecType::IMMEDIATE

			add_primitive("make-exec") do |int|
				last_xt = int.dictionary.last_xt
				int.dictionary[last_xt] = lambda do |other_int|
					other_int.int_enter
					other_int.int_next
				end
			end

			add_colon(":", "create", "make-exec", "dolit", 1, "state", "!")

			add_primitive("compile_exit") do |int|
				int.dictionary.insert_here(int.dictionary.get_xt("exit"))
			end

			add_colon(";", "dolit", 0, "state", "!", "compile_exit").exec_type = Forth::ExecType::IMMEDIATE

      add_primitive("accept") do |int|
				limit = int.data_stack.pop
				addr = int.data_stack.pop
				in_string = $stdin.gets("\n",limit)
				int.dictionary.insert_at(addr, in_string.bytes)
				int.data_stack.push(in_string.length)
      end

			add_colon("variable", "create", "dolit", 0, ",")

			add_constant("bl", ' '.ord)
			add_constant("qt", '"'.ord)
			add_constant("cr", "\n".ord)

			add_primitive("find") do |int|
				cstr = int.data_stack.pop
				len = int.dictionary[cstr]
				str = Forth.counted_string_to_string(int.dictionary[cstr..cstr+len])
				xt = int.dictionary.get_xt(str)
				if xt == -1
					int.data_stack.push cstr
					int.data_stack.push 0
					return
				end
				exec_type = int.dictionary[xt-1]
				int.data_stack.push xt
				int.data_stack.push( exec_type == Forth::ExecType::IMMEDIATE ? 1 : -1 )
			end

			add_colon("refill", "tib", "@", "dup", ">in", "!", "dolit", 80, "accept")

			add_primitive("parse") do |int|
				delim = int.data_stack.pop
				parse_result = hard_parse(int, delim)
				cur_word = parse_result[:string]
				int.data_stack.push(parse_result[:addr])
				int.data_stack.push(parse_result[:length])
				int.set_variable(">in", int.get_variable(">in") + 1) unless cur_word.length <= 1
			end

			add_primitive("'") do |int|
				xt_name = hard_parse(int, " ".ord)[:string].strip
				int.data_stack.push(int.dictionary.get_xt(xt_name))
			end

			add_primitive("execute") do |int|
				xt = int.data_stack.pop
				int.int_call xt
			end

			add_primitive("move") do |int|
				size = int.data_stack.pop
				dest = int.data_stack.pop
				src = int.data_stack.pop
				return if size > 0
				int.dictionary.bytes[dest..dest+size] = int.dictionary.bytes[src..src+size]
			end

			add_primitive("quit") do |int|
				delim = int.data_stack.pop
				while int.get_variable(">in") < (int.get_variable("tib") + 80) do
					cur_word = hard_parse(int, delim)[:string].strip
					puts "CURRENT WORD: #{cur_word} CURRENT STATE: #{int.get_variable("state")}"

					if cur_word.numeric?
						cur_entry = cur_word.to_i
					else
						cur_entry = DictionaryEntry.new(int.dictionary, int.dictionary.search(cur_word))
						puts "EXEC_TYPE: #{cur_entry.exec_type}"
					end
					cur_state = int.get_variable("state")
					if cur_state != 0 # compile
						if cur_entry.is_a?(Integer)
							puts "Compiling...\n\n"
							int.dictionary.insert_here(int.dictionary.get_xt("dolit"))
							int.dictionary.insert_here(cur_entry)
							next
						end
						case cur_entry.exec_type
						when Forth::ExecType::NORMAL
							puts "Compiling...\n\n"
							int.dictionary.insert_here cur_entry.xt_pos
						when Forth::ExecType::IMMEDIATE, Forth::ExecType::COMPILE_ONLY
							puts "Interpreting directly...\n\n"
							int.int_call(cur_entry.xt_pos)
						end
					else # interpret
						puts "Interpreting directly...\n\n"
						if cur_entry.is_a?(Integer)
							int.data_stack.push(cur_entry)
							next
						end

						case cur_entry.exec_type
						when Forth::ExecType::NORMAL, Forth::ExecType::IMMEDIATE
							int.int_call(cur_entry.xt_pos)
						end
					end
				end
				int.clear_tib
			end

			add_primitive("infinite-loop") do |int|
				int.return_stack[-1] = int.pc - 1 if int.return_stack.size > 0
				int.return_stack.push(int.pc - 1) if int.return_stack.size < 1
				puts "===REPEAT==="
				int.stack_trace
			end

			add_primitive("clear") do |int|
				int.data_stack = []
			end

			add_primitive("t{") do |int|
				int.data_stack = []
			end

			add_primitive("->") do |int|
				int.return_stack.push int.dictionary.here
				int.return_stack.push int.data_stack.size
				int.dictionary.insert_here int.data_stack if int.data_stack.size > 0
				int.data_stack = []
			end

			add_primitive("}t") do |int|
				old_size = int.return_stack.pop
				old_addr = int.return_stack.pop
				old_stack = int.dictionary.bytes[old_addr..old_addr+old_size]
				cur_size = int.data_stack.size
				cur_stack = int.data_stack
				if old_size != cur_size
					puts "Wrong number of results." if old_size != cur_size
				elsif old_stack != cur_stack
					puts "Incorrect results"
				else
					puts "Pass"
				end
			end

			add_primitive("postpone") do |int|
				cur_word = hard_parse(int, " ".ord)
				
			end
			
			add_colon("colon_init", "infinite-loop", "refill", "drop", "bl", "quit")

    end
  end

end
